#!/bin/bash

# Installs the basic needed packages
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y build-essential binutils-doc autoconf flex bison libjpeg-dev
sudo apt-get install -y libfreetype6-dev zlib1g-dev libzmq3-dev libgdbm-dev libncurses5-dev
sudo apt-get install -y automake libtool libffi-dev curl git tmux gettext
sudo apt-get install -y nginx
sudo apt-get install -y rabbitmq-server redis-server
sudo apt-get install -y circus
sudo apt-get install -y postgresql postgresql-contrib postgresql-dev
sudo apt-get install -y python3 python3-pip python-dev python3-dev python-pip virtualenvwrapper
sudo apt-get install -y libxml2-dev libxslt-dev
sudo apt-get install -y libssl-dev libffi-dev

# Adds the Taiga user
sudo adduser taiga
sudo adduser taiga sudo
sudo -u taiga mkdir /home/taiga/logs
sudo -u postgres createuser taiga
sudo -u postgres createdb taiga -O taiga --encoding='utf-8' --locale=en_US.utf8 --template=template0

# Configured RabbitMQ
sudo rabbitmqctl add_user taiga PASSWORD_FOR_EVENTS
sudo rabbitmqctl add_vhost taiga
sudo rabbitmqctl set_permissions -p taiga taiga ".*" ".*" ".*"

# Clone the taiga-back repo
sudo su taiga
cd /home/taiga
git clone https://github.com/taigaio/taiga-back.git taiga-back
cd taiga-back
git checkout stable

mkvirtualenv -p /usr/bin/python3 taiga
pip install -r requirements.txt
python manage.py migrate --noinput
python manage.py loaddata initial_user
python manage.py loaddata initial_project_templates
python manage.py compilemessages
python manage.py collectstatic --noinput

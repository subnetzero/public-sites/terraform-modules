# This deploys both taiga front and backend to a single server
resource "aws_instance" "taiga" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.instance_type}"

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
  }

  tags {
    Name = "taiga"
  }
}

data "template_file" "localspy" {
  template = "${file("${path.module}/templates/local.py.tpl")}"

  vars {
    scheme = "${var.scheme}"
    domain = "${var.domain}"
    rabbitmq_password = "${var.rabbitmq_password}"
    secret_key = "${var.secret_key}"
  }
}

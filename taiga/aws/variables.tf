# Instance type to host the Taiga front end, back end, and DB

variable "domain" {}

# This will be the password for the rabbitmq user taiga
variable "rabbitmq_password" {}

# This is the secret key for Django. You can generate one with `pwgen 30` or similar
variable "secret_key" {}

variable "instance_type" {
  default = "t2.medium"
}

variable "root_volume_type" {
  default = "gp2"
}

variable "root_volume_size" {
  default = "32"
}

variable "allow_public_registration" {
  default = "True"
}

variable "scheme" {
  default = "https"
}
